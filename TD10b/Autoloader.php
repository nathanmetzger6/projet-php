<?php
  class Autoloader{
    private $prefixe;
    private $chemin;
    public function __construct($p,$ch){
        $this->prefixe=$p;
        $this->chemin=$ch;
        $this->register();
    }
    public function loadClass($nomclasse){
      $res=substr($nomclasse,0,strlen($this->prefixe));
      if($res == $this->prefixe){
        $res2=str_replace('\\',DIRECTORY_SEPARATOR,substr($nomclasse,strlen($this->prefixe)));
        require$this->chemin.$res2.'.php';
      }
    }
    public function register(){
      spl_autoload_register([$this,'loadClass']);
    }


  }
