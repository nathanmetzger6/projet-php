<?php
/**
 * File:  index.php
 * Creation Date: 04/12/2017
 * description:
 *
 * @author: canals
 */


// connection BD


require_once __DIR__ . '/vendor/autoload.php';
require_once 'vendor/autoload.php' ;
use \mywishlist\controleur\ControleConsult ;
use \mywishlist\controleur\ControleCrea;
use \mywishlist\vue\VueIndex;
use \mywishlist\vue\VueFormulaire;
use \mywishlist\vue\VueListe;
use \mywishlist\controleur\ControleCompte;
use mywishlist\controleur\ControleModif;
use mywishlist\controleur\ControleParticip;

use Illuminate\Database\Capsule\Manager as DB;
session_start();
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app=  new \Slim\Slim();

$app->get('/',function(){
	echo VueIndex::render();
})->name('racine');

$app->get('/liste/:no/:noItem',function($no,$noItem) {//ninon
	$cs=new ControleConsult($no);
	$cs->choixListeItem($noItem);
})->name('choix_liste_Item');

$app->get('/modifItem/:token/:item',function($token, $item){
    $cm=new ControleModif($token,$item);
    $cm->render();
});
$app->post('/modifItem/:token/:item',function($token, $item) use ($app){
    $url= $app->urlFor('racine').'liste/'.$token;
    try {
        $cm=new ControleModif($token,$item);
        $cm->modifier();

    }
    catch (Exception $e){
        echo $e;
    }
    $app->response->redirect($url);
});
$app->get('/supItem/:token/:item',function($token, $item) use ($app){
    $url= $app->urlFor('racine').'liste/'.$token;
    try {
        $cm=new ControleModif($token,$item);
        $cm->supprimer();
    }
    catch (Exception $e){

    }
    $app->response->redirect($url);
});

$app->get('/partagerListe/:liste',function($no) {
	$cs=new ControleCrea($no);
	$cs->partageListe();
})->name('partage_liste');

$app->get('/listesPubliques/:liste',function($liste) {
	echo " rendre la liste publique ";
})->name('rendreListePublique');

$app->post('/liste/:token/:nbListe',function($nbListe) {
	echo "nb liste selon le token";
});

$app->get('/creerListe', function(){//nathan
	$cs=new ControleCrea();
	$cs->formulaireListe();
})->name('FormListe');

$app->post('/creerListe', function() {//nathan
	$cs=new ControleCrea();
	$cs->enregistrerListe();
})->name('PostListe');





$app->post('/participCagnotte/:token/:no', function() {
echo 'test';
})->name('particip_cagnotte');

$app->post('/creerCagnotte/:token', function() {
echo 'test';
})->name('creer_cagnotte');


$app->post('/reserverItem/:token/:no', function($token,$no) use ($app){
    $cp=new ControleParticip($no,$token);
    try{
        $cp->enregistrerParticip();
        $url= $app->urlFor('racine');
        $app->response->redirect($url.'/liste/'.$token);
    }
    catch (Exception $e){
    }
})->name('reserver_item');


$app->post('/adjItem/:user_id', function($user_id) use ($app){
    $cs=new ControleCrea();
    try{
        $cs->enregistrerItem($user_id);
        $url= $app->urlFor('racine');
        $app->response->redirect($url.'/liste/'.$user_id);
    }
    catch (Exception $e){
        print_r($e);
    }
})->name("adjItem");



$app->get('/liste/:no', function($no) { //nathan
	$cs=new ControleConsult($no);
	$cs->choixListe();
	//echo $cs->render(0);
})->name('choix_liste');

/*$app->get('/liste/:no', function($no) {
	echo "chemin OK";
	echo "methode ok";
});*/

$app->get('/Inscription',function(){
    $vF= new VueFormulaire();
    echo $vF->inscription();
})->name('Inscription');
$app->post('/Inscription',function() use ($app){
   $cI=new ControleCompte();
   $cI->inscription();
   $app->response->redirect($app->urlFor("Connexion"));
});

$app->get('/Connexion',function(){
	$vF= new VueFormulaire();
	echo $vF->connexion();
})->name('Connexion');

$app->post('/Connexion',function() use ($app){
$cI=new ControleCompte();
$bool=$cI->seConnecter();
if($bool==1){
	$app->response->redirect($app->urlFor("racine"));
}else {
	$app->response->redirect($app->urlFor("Connexion"));
}
});

$app->get('/Deconnexion',function(){
	$vF= new VueFormulaire();
	echo $vF->deconnexion();
})->name('Deconnexion');

$app->post('/Deconnexion',function() use ($app){
	$cI=new ControleCompte();
	$cI->seDeconnecter();
	$app->response->redirect($app->urlFor("racine"));
});

$app->get('/Compte',function(){
	$vF= new VueFormulaire();
	echo $vF->monCompte();
})->name('Compte');

$app->post('/Compte',function() use ($app){
	$app->response->redirect($app->urlFor("ModifCompte"));
	//$cI=new ControleCompte();
	//$cI->modifierInfos();
});

$app->get('/ModifCompte',function(){
	$vF= new VueFormulaire();
	echo $vF->modifCompte();
})->name('ModifCompte');

$app->post('/ModifCompte',function() use ($app){
	$cI=new ControleCompte();
	$cI->modifierInfos();
	$app->response->redirect($app->urlFor("Compte"));
});

$app->get('/ListeCree',function(){
	$vF= new VueListe(0);
	echo $vF->afficherListeListes(2);
})->name('ListeCree');

$app->post('/ListeCree',function() use ($app){
	$app->response->redirect($app->urlFor("LierListe"));
});


$app->post('/LierListe',function() use ($app){
	$vF= new ControleCompte();
	echo $vF->lierListe();
	//$app->response->redirect($app->urlFor("ListeCree"));
})->name('LierListe');


$app->get('/ListePublique',function(){
	$vF= new VueListe(0);
	echo $vF->afficherListeListes(1);
})->name('ListePublique');

$app->get('/ListeParticip',function(){
	$vF= new VueListe(0);
	echo $vF->listeItemParticip();
})->name('ListeParticip');

$app->run();
