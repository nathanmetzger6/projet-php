<?php
//controle des compte et associé
namespace mywishlist\controleur;
use mywishlist\modele\Compte;
use mywishlist\modele\Creation;
use mywishlist\modele\Liste;

class ControleCompte
{
    const LongMin= 8;
    public function __construct(){

    }
    public function inscription(){
        $app = \Slim\Slim::getInstance();
        $url = $url = $app ->urlFor('racine');
        $nomUtil=$app->request->post('nomUtil');
        $nom=$app->request->post('nom');
        $prenom=$app->request->post('prenom');
        $mdp1=$app->request->post('MDP1');
        $mdp2=$app->request->post('MDP2');
        $email=$app->request->post('Email');
        $anniversaire=$app->request->post('DateA');
        $nomUtil=filter_var($nomUtil, FILTER_SANITIZE_STRING);
        $nom=filter_var($nom, FILTER_SANITIZE_STRING);
        $prenom=filter_var($prenom, FILTER_SANITIZE_STRING);
        $compte= new Compte();
        $compte->mdp= password_hash($mdp1,PASSWORD_DEFAULT, ['cost'=> 12]);
        $compte->nomUtil=$nomUtil;
        $compte->nom=$nom;
        $compte->prenom=$prenom;
        $compte->email=$email;
        $compte->anniversaire=$anniversaire;
        $compte->save();
    }

    public function seConnecter(){
      $app = \Slim\Slim::getInstance();
      $url = $url = $app ->urlFor('racine');
      $nomUtil=$app->request->post('nomUtil');
      $mdp=$app->request->post('MDP');
      $nomUtil=filter_var($nomUtil, FILTER_SANITIZE_STRING);
      $compte= Compte::where('nomUtil','=',$nomUtil)->first();
      if(!is_null($compte)){
        if (password_verify($mdp, $compte->mdp)){
            $_SESSION['compte']=$nomUtil;
            $bool=1;
        }else{
          $bool=0;
        }
      }else{
        $bool=0;
      }
      return $bool;
    }

    public function seDeconnecter(){
        unset($_SESSION['compte']);
    }

    public function modifierInfos(){
        if($this::verifDroit()){
          $app = \Slim\Slim::getInstance();
          $url = $url = $app->urlFor('racine');
          $compte= Compte::where('nomUtil','=',$_SESSION['compte'])->first();
          $mdp0=$app->request->post('MDP0');
          if(password_verify($mdp0, $compte->mdp)){
            $nom=$app->request->post('nom');
            $prenom=$app->request->post('prenom');
            $mdp1=$app->request->post('MDP1');
            $mdp2=$app->request->post('MDP2');
            $email=$app->request->post('mail');
            $anniversaire=$app->request->post('anniv');
            $nom=filter_var($nom, FILTER_SANITIZE_STRING);
            $prenom=filter_var($prenom, FILTER_SANITIZE_STRING);
            $compte->mdp= password_hash($mdp1,PASSWORD_DEFAULT, ['cost'=> 12]);
            $compte->nom=$nom;
            $compte->prenom=$prenom;
            $compte->email=$email;
            $compte->anniversaire=$anniversaire;
            $compte->save();
          }
        }
    }

    public function lierListe(){
      if($this::verifDroit()){
        $app = \Slim\Slim::getInstance();
        $url = $url = $app->urlFor('racine');
        $urlliste=$app->request->post('lien');
        $urlliste=filter_var($urlliste,FILTER_VALIDATE_URL);
        $array= explode('/',$urlliste);
        $test=false;
        $res=null;
        foreach($array as $s){
            if ($s == 'liste'){
                $test=true;
            }
            elseif ($test){
                $res=$s;
                break;
            }
          }
        if (!is_null($res)){
            $l=Liste::where("user_id", "=", $res)->first();
            if (!is_null($l)){
                $creation= new Creation();
                $creation->noliste=$l->no;
                $creation->id_compte=$_SESSION['compte'];
            }
        }
      }
    }

    public static function verifDroit(){
        return (!is_null($_SESSION['compte']));
    }
}
