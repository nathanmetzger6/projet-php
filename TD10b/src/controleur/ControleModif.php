<?php
// controleur des modification apportées aux listes et items
namespace mywishlist\controleur;
use mywishlist\modele\Item;
use mywishlist\modele\Liste;
use \mywishlist\vue\VueListe ;
use mywishlist\vue\VueFormulaire;
use Slim\Slim;

class ControleModif{
    private $user_id;
    private $idItem;

    public function __construct($user, $id){
        try {
            Liste::where("user_id","=",$user)->firstOrFail();
            Item::where("id","=",$id)->firstOrFail();
            $this->user_id=$user;
            $this->idItem=$id;
        }
        catch (\Exception $e){
            throw new \Exception("l'item ou l'utilisateur n'existe pas");
        }
    }

    public function supprimer(){
        $item= Item::where("id","=",$this->idItem)->delete();
    }

    public function modifier(){
        $app=Slim::getInstance();
        $item = Item::where("id","=",$this->idItem)->first();
        $test=false;
        if (!empty($_POST["nom"])){
            $item->nom= filter_var($app->request->post('nom'), FILTER_SANITIZE_STRING);
            $test=true;
        }
        if (!empty($_POST["description"])){
            $item->descr= filter_var($app->request->post('description'), FILTER_SANITIZE_STRING);
            $test=true;
        }
        $imgNom=null;
        if (isset($_FILES['image'])) {
            $Inter= pathinfo($_FILES['image']['name']);
            $imgNom=$_FILES['image']['name'];
            $imgNom=filter_var($imgNom, FILTER_SANITIZE_STRING);
            if (in_array($Inter['extension'], ControleCrea::AUTH_EXT)) {
                move_uploaded_file($_FILES['image']['tmp_name'], "img/" . $imgNom);
                $test=true;
                if (file_exists("img/".$item->img)){
                    unlink("img/".$item->img);
                }
                $item->img= $imgNom;
            }
            else {
                throw new \Exception("l'extension de l'image n'est pas bonne");
            }
        }
        if (!empty($_POST["prix"])){
            $item->tarif= filter_var($app->request->post('prix'),FILTER_SANITIZE_NUMBER_INT);
            $test=true;
        }
        if ($test){
            $item->save();
        }
    }
    
    public function render(){
        $vueForm=new VueFormulaire();
        echo $vueForm->modifierItem(Item::where("id","=",$this->idItem)->first(), $this->user_id);
    }

}
