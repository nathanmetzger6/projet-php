<?php
// controleur de la participation à une liste de souhait
namespace mywishlist\controleur;
use mywishlist\modele\Reserv;
use \mywishlist\modele\Item;
use \mywishlist\modele\Liste;
class ControleParticip{
    protected $itemId;
    protected $token;

    public function __construct($itemId, $token){
        $itemIdFilter=filter_var($itemId, FILTER_SANITIZE_NUMBER_INT);
        if (!is_null(Item::where('id','=',$itemIdFilter))){
            $this->itemId=$itemIdFilter;
        }
        else {
            throw new \Exception();
        }
        $this->token=filter_var($token, FILTER_SANITIZE_STRING);
    }

    public function enregistrerParticip(){
        $app = \Slim\Slim::getInstance();
        $list= Liste::where('token','=',$this->token)->first();
        if (!is_null($list)){
            $reserv = new Reserv();
            $reserv->item=$this->itemId;
            $reserv->message=filter_var($app->request->post("message"), FILTER_SANITIZE_STRING);
            if(isset($_SESSION['compte']))
              $reserv->no_user=$_SESSION['compte'];
            else
              $reserv->no_user=filter_var($app->request->post('numUtil'), FILTER_SANITIZE_STRING);
            if (!is_null($reserv->message) && !is_null($reserv->no_user)){
                try {
                    $reserv->save();
                }
                catch (\Exception $e){
                }
            }
            else{
                throw new \Exception();
            }
        }
        else {
            throw new \Exception();
        }
    }
}
