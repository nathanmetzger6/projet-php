<?php
// controleur de consultation de liste
namespace mywishlist\controleur;
use \mywishlist\modele\Liste;
use \mywishlist\vue\VueListe;
class ControleConsult{
	protected $user;

	public function __construct($u){
			$this->user=$u;
	}

	public function enregistrerListe(){
			$app = \Slim\Slim::getInstance();
			$aff = new VueListe(VueListe::NEWLIST,$liste);
			$titre=$app->request->post('titre');
			$liste = new Liste();
			$liste->titre = filter_var($titre, FILTER_SANITIZE_STRING);
			$liste->expiration= filter_var($app->request->post('expire'),FILTER_SANITIZE_INT);
			$liste->no=$this->user;
			$liste->save();
	}

	public function saveListe(){
			var_dump($_POST);
			$titre= filter_var($_POST['titre'], FILTER_SANITIZE_STRING);
			$liste= new Liste();
			$liste->titre=$titre;
			$liste->save();
			$aff= new VueListe(0,0);
			echo $aff->render(33);
	}

	public function choixListe(){
			$liste = Liste::where('user_id','=', $this->user)->first();
			if(!is_null($liste)){
					$aff = new VueListe(0,$liste);
					echo $aff->render(0);
			}
			else {
          $liste = Liste::where('token','=', $this->user)->first();
          if(!is_null($liste)) {
              $aff = new VueListe(3, $liste);
              echo $aff->render(3);
          }
          else{
              $aff= new VueListe(404);
              echo $aff->render(404);
          }
			}
	}

	public function choixListeItem($noIt){
        $test=array(Liste::where('token','=', $this->user)->first(), 4);
        $test2=array(Liste::where('user_id','=', $this->user)->first(), 1);
        $liste= is_null($test2[0])?
            $test  :  $test2;
				if(!is_null($liste)){
					$item=$liste[0]->items()->where('id','=',$noIt)->first();
					if(!is_null($item)){
						$aff = new VueListe(0,$liste[0],$item);
						echo $aff->render($liste[1],$item,$liste[0]->token);
					}
					else{
						$aff = new VueListe(403);
						echo $aff->render(403);
					}
				}
				else {
						$aff = new VueListe(404);
						echo $aff->render(404);
				}
	}
}
