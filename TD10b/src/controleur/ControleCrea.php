<?php
// controleur de la creation des listes/items
namespace mywishlist\controleur;
use mywishlist\modele\Item;
use mywishlist\modele\Creation;
use \mywishlist\modele\Liste ;
use \mywishlist\vue\VueFormulaire ;
use \mywishlist\vue\VueListe ;

class ControleCrea{
    const AUTH_EXT =array("jpeg","jpg","png");
    public function __construct($u=0){
        $this->user=$u;
    }

    public function formulaireListe(){
        if (is_null($this->user)){
        }
        else {
            $aff = new VueFormulaire();
            echo $aff->render();
        }
    }

    public function enregistrerListe(){
        $app = \Slim\Slim::getInstance();
        $titre=$app->request->post('titre');
        $description=$app->request->post('description');
        $liste = new Liste();
        $liste->titre = filter_var($titre, FILTER_SANITIZE_STRING);
        $liste->description = filter_var($description, FILTER_SANITIZE_STRING);
        $liste->expiration= filter_var($app->request->post('expiration'),FILTER_SANITIZE_NUMBER_INT);
        $liste->token=bin2hex(openssl_random_pseudo_bytes(8));
        if(isset($_SESSION['compte'])){
          $liste->user_id=$_SESSION['compte'];
          $liste->save();
          $crea=new Creation();
          $crea->noliste=$liste->no;
          $crea->id_compte=$liste->user_id;
          $crea->save();
        }else{
          $liste->user_id=bin2hex(openssl_random_pseudo_bytes(8));
          $liste->save();
        }
        $liste= Liste::where('titre','=', $liste->titre)->first();
        $aff = new VueFormulaire(1, $liste);
        echo $aff->render(VueFormulaire::ADJITEMS);
    }

    public function enregistrerItem($user){
        $app = \Slim\Slim::getInstance();
        $liste=Liste::where('user_id','=', $user)->first();
        if (!is_null($liste)){
            $item = new Item();
            $item->liste_id = $liste->no;
            $item->nom= filter_var($app->request->post('nom'), FILTER_SANITIZE_STRING);
            $item->descr= filter_var($app->request->post('description'), FILTER_SANITIZE_STRING);
            $imgNom=null;
            $Inter= pathinfo($_FILES['image']['name']);
            if (isset($_FILES['image'])) {
                $imgNom=$_FILES['image']['name'];
                echo "test".is_null($imgNom);
                if (in_array($Inter['extension'], self::AUTH_EXT)) {
                    move_uploaded_file($_FILES['image']['tmp_name'], "img/" . $imgNom);
                }
                else {
                    throw new \Exception("l'extension de l'image n'est pas bonne");
                }
            }
            $item->img= filter_var($imgNom, FILTER_SANITIZE_STRING);
            $item->tarif= filter_var($app->request->post('prix'),FILTER_SANITIZE_NUMBER_INT);
            $item->save();
        }
    }
}
