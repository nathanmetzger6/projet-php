<?php
//modelise les items d'une liste
namespace mywishlist\modele;
class Item extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'item';
  protected $primaryKey = 'id' ;
  public $timestamps = false ;
  public function liste() {
    return $this->belongsTo('\mywishlist\modele\Liste','liste_id') ;
  }
    public function estReserve()
    {
        if (is_null($this->hasMany('\mywishlist\modele\Reserv', 'item')->first())){
            return false;
        }
        else{
            return true;
        }
    }
    public function reservation(){
       return $this->hasMany('\mywishlist\modele\Reserv', 'item')->first();
    }

 public function formulaire(){
     return <<<Fin
        <form class="formReserv" method="post" action="#">
                <p> Nom d'utilisateur : <input type="text" name="numUtil" /></p>
                <p> Message :  <input type="text" name="message" /></p>
                <p> <input type="submit" value="ok"></p>
             </form>
Fin;
 }
}
