<?php
//modelise les listes de souhaits
namespace mywishlist\modele;
class Liste extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'liste';
  protected $primaryKey = 'no';
  public $timestamps = false ;
  public function items() {
    return $this->hasMany('\mywishlist\modele\Item','liste_id') ;
 }



}
