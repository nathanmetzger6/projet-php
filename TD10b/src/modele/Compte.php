<?php
//modelise un compte participant et createur
namespace mywishlist\modele;
class Compte extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'compte';
  protected $primaryKey = 'nomutil';
  public $timestamps = false ;
  public function items() {
    return $this->hasMany('\mywishlist\modele\Creation','id_compte') ;
 }



}
