<?php
//modelise le lien entre un compte et les listes créees par celui ci
namespace mywishlist\modele;
class Creation extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'creation';
  protected $primaryKey = 'noliste' ;
  public $timestamps = false ;
  public function liste() {
    return $this->belongsTo('\mywishlist\modele\Compte','id_compte') ;
  }
}
