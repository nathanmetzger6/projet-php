<?php
//modelise les reservations effectuées sur un item
namespace mywishlist\modele;
class Reserv extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'reservation';
    protected $primaryKey = 'item';
    public $timestamps = false;
    public function item() {
        return $this->belongsTo('\mywishlist\modele\Item','id') ;
    }
}
