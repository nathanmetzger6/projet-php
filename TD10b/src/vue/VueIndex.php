<?php
//la vue de la page d'acceuil
namespace mywishlist\vue;
use \mywishlist\vue\header;

class VueIndex {
  public static function render() {
    $app = \Slim\Slim::getInstance();
    $route=$app->urlFor("racine");
    $content=header::header(2);
    $html = <<<FIN
                $content
                <div class="fond"></div>
                <div class="section">
                  <div class="title"><a href="$route"<h1 class="titreH"> MyWishList</h1></a></div>
                </div>

            </body>
        </html>
FIN;
    return $html;
  }
}
