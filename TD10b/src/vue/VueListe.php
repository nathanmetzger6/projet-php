<?php
// vue des affichages des listes
namespace mywishlist\vue;
use \mywishlist\modele\Item;
use \mywishlist\modele\Reserv;
use \mywishlist\modele\Compte;
use \mywishlist\modele\Liste;
use \mywishlist\modele\Creation;

class VueListe{
    const AFFICHELISTE=0;
    const NEWLIST=1;
    protected $html;
    protected $role;
    protected $liste;
    protected $item;

    public function __construct($r, $liste=null,$item=null){
        $this->role=$r;
        $this->liste=$liste;
        $this->item=$item;
    }

    public function afficherListeNewItem(){
        $this->html .= "<h2>{$this->liste->titre}</h2>";
        $this->html .=Item::creerItem();
    }

    public function afficherListeListes($type){
        $app = \Slim\Slim::getInstance();
        $routeLier= $app->urlFor('LierListe');
        $route=$app->urlFor("racine")."liste/";
        $this->html.=header::header();
        $url =$app -> urlFor( "racine");
        switch ($type){
            case 1:
                $app = \Slim\Slim::getInstance();
                $route=$app->urlFor("ListePublique");
                $listes=Liste::where('plublique','=','oui')->get();
                if(is_null($listes))
                  $this->html.=$this->render(401);
                else{
                  foreach ($listes as $l) {
                    $urlliste=$url."/liste/".$l->user_id;
                    $this->html.=$this->afficheurListe($l,$urlliste);

                  }
                }
            case 2 :
            $app = \Slim\Slim::getInstance();
            $route=$app->urlFor("ListeCree");
            if (isset($_SESSION['compte'])){
              $crea=Creation::where('id_compte','=',$_SESSION['compte'])->get();
              if(is_null($crea))
                $this->html.=$this->render(402);
              else{
                foreach ($crea as $c) {
                  $liste=Liste::where('no','=',$c->noliste)->first();
                  $urlliste=$url."/liste/".$liste->token;
                  $this->html.=$this->afficheurListe($liste,$urlliste);
                }
              }
            }
          break;
        }
        $this->html.=<<<FIN
       <form class="lier" method="post" action="$routeLier">
            <p>Entrer un lien :
            <input type="text" name="lien" required></p>
            <input type="submit" value="Lier une liste" class="button">
        </form>
FIN;
        return $this->html;
      $this->html.=header::header();
      switch ($type){
        case 1:
          $app = \Slim\Slim::getInstance();
          $route=$app->urlFor("ListePublique");
          $listes=Liste::where('plublique','=','oui')->get();
          if(is_null($listes))
            $this->html.=$this->render(401);
          else{
            foreach ($listes as $l) {
              $this->html.=$this->afficheurListe($l);
            }
          }
        case 2 :
          $app = \Slim\Slim::getInstance();
          $route=$app->urlFor("ListeCree");
          if (isset($_SESSION['compte'])){
            $crea=Creation::where('id_compte','=',$_SESSION['compte'])->get();
            if(is_null($crea))
              $this->html.=$this->render(402);
            else{
              foreach ($crea as $c) {
                $liste=Liste::where('no','=',$c->noliste)->first();
                $this->html.=$this->afficheurListe($liste);
              }
            }
          }
        break;
      }
}

    public function listeItemParticip(){
      $this->html.=header::header();
      $app = \Slim\Slim::getInstance();
      $route=$app->urlFor("ListeParticip");
      if (isset($_SESSION['compte'])){
        $reserv=Reserv::where('no_user','=',$_SESSION['compte'])->get();
        if(is_null($reserv))
          $this->html.=$this->render(401);
        else{
          foreach ($reserv as $r) {
            $item=Item::where('id','=',$r->item)->first();
            $this->html.=$this->afficheurItem($item);
          }
        }
      }
      return $this->html;
    }

    private function afficheurItem($i){
      $app = \Slim\Slim::getInstance();
      $routeImg=$app->urlFor("racine")."img";
      $res=<<<FIN
    <li class="item">
      <img src="$routeImg/$i->img" width="60" height="60">
      $i->nom -
      $i->descr
FIN;
      return $res;
    }

    private function afficheurListe($liste,$url){
      $app = \Slim\Slim::getInstance();
      $res=<<<FIN
<div class="section">
<li class="item">
  <a href=$url><h4>$liste->titre</h4></a>
  <p>$liste->description -</p>
  <p>$liste->expiration</p>
</li>
FIN;
      return $res;
    }

    public function afficherListes($select=0){
        $app = \Slim\Slim::getInstance();
        $route=$app->urlFor("choix_liste");
        $route=str_replace(":no","",$route);
        $route.=$this->liste->no;
        $this->html = "<div class=\"section\"><h2 class=\"titreL\">{$this->liste->titre}</h2>";
        $items=$this->liste->items()->get();
        $this->html.= '<ul class="items">';
        foreach ($items as $item) {
            $this->html .= $this->afficherItem($item,$select);
        }
        $this->html.= '</ul>';
        if ($select==0){
            $url = $app ->urlFor('racine');
            $urlP =$url."liste/".$this->liste->token;
            $this->html.= <<<FIN
                <h3>
                    votre lien de partage est <a href="$urlP">$urlP</a>
                </h3>
FIN;
        }
        return $this->html;
    }

    public function afficherItem($item, $select){
        $app = \Slim\Slim::getInstance();
        $routeLien=$app->urlFor("racine")."liste";
        $routeImg=$app->urlFor("racine")."img";
        $routeModif=$app->urlFor("racine")."modifItem/";
        $routeSup=$app->urlFor("racine")."supItem/";
        $lien=($select==0)?$this->liste->user_id:$this->liste->token;
        $res=<<<FIN
      <li class="item">
        <img src="$routeImg/$item->img" width="60" height="60">
        $item->nom -
        $item->descr
FIN;
        $reserv= $item->reservation();
        if ($select!=0 && !$item->estReserve()){
            $res.="<a href=".$routeLien."/".$this->liste->token."/".$item->id." >reserver</a>";
        }
        else if ($select==0 && $item->estReserve() && time()-strtotime($this->liste->expiration)>0){
            $res.="<h3> Reservé par : $reserv->no_user</h3> <br/><h5> Avec le message : $reserv->message</h5>";
        }
        else if ($item->estReserve() && $select!=0){
            $res.="<h5> L item est reserve par: $reserv->no_user</h5>";
        }
        else if ($select==0){
            $res.="<a href=".$routeModif.$this->liste->user_id."/".$item->id." ><img src='$routeImg/modif.png' width=\"60\" height=\"60\" ></a>";
            $res.="<a href=".$routeSup.$this->liste->user_id."/".$item->id." ><img src='$routeImg/sup.png' width=\"60\" height=\"60\"></a>";
        }
        return $res."</li>";
    }

    public function afficherListeItem($item,$select=0){
      $app = \Slim\Slim::getInstance();
      $route=$app->urlFor("racine");
      $this->html = "<h2 class=\"titreL\">{$this->liste->titre}</h2>";
      $this->html.= '<ul class="item">';
      $this->html .= $this->afficherItem($item,$select);
      $this->html.= '</ul>';
      if ($select!=0 && !$item->estReserve()){
          $this->html.=$item->formulaire();
      }
      elseif ($select==0 && !$this->item->estReserve())
      return $this->html;
    }

    private function formListe(){
        $content = <<<FIN
        <form action="http://localhost/test/mywishlist/nouvelle_liste2" method="post">
            <input type="text" name="titre"/>
            <input type="text" name="description"/>
            <input type="date" name="expiration"/>
            <button name="btn1" value="val1">Valider</button>
        </form>
FIN;
        return $content;
    }

    private function listeNoTrouver(){
        return "<h1 class='erreur'> 404 erreur: la liste que vous avez demandé n'existe pas</h1>";
    }
    private function itemNoTrouver(){
        return "<h1 class='erreur'> 404 erreur: l'item que vous avez demandé n'existe pas dans la liste</h1>";
    }
    private function NoListesCrees(){
        return "<h1 class='erreur'> Vous n'avez pas encore creer de listes</h1>";
    }
    private function NoListesParticipee(){
        return "<h1 class='erreur'> Vous n'avez pas encore participer à des listes</h1>";
    }

    public function adjItem(){
        $app = \Slim\Slim::getInstance();
        $url = $app ->urlFor('racine');
        $url.="adjItem/".$this->liste->user_id;
        return <<<FIN
            <form enctype="multipart/form-data" class="adjItem" method="post" action="$url">
                    <p> Nom de l'item <input type="text" name="nom" /> </p>
                    <p> Ajoutez une image<input name="image" type="file" /></p>
                    <p>Description : <input name="description" type="text" /></p>
                    <p>Tarif : <input name="prix" type="text"></p>
                    <input type="submit" value="ajouter" class="button">
            </form>
          </div>
FIN;
    }

    public function formulaire($token,$no){
        $app = \Slim\Slim::getInstance();
        $url = $app ->urlFor('racine')."reserverItem";
        if(isset($_SESSION['compte'])){
          return <<<FIN
          <form class="formReserv" method="post" action="$url/$token/$no">
                  <p> Message :  <input type="text" name="message" required /></p>
                  <p> <input type="submit" value="ok"></p>
               </form>
FIN;
        }else{
          return <<<FIN
          <form class="formReserv" method="post" action="$url/$token/$no">
                  <p> Nom d'utilisateur : <input type="text" name="numUtil" required /></p>
                  <p> Message :  <input type="text" name="message" required /></p>
                  <p> <input type="submit" value="ok"></p>
               </form>
FIN;
        }
    }

    public function render($select,$item=null,$token=null){
        $content=header::header();
        switch($select){
            case 0 :
                $content.=$this->afficherListes().$this->adjItem();
            break;
            case 1 :
                $content.=$this->afficherListeItem($item);
            break;
            case 2 :
                $content.=$this->formListe();
            break;
            case 3 :
                $content.=$this->afficherListes( 2);
                break;
            case 4 :
                $content.=$this->afficherListeItem($item).$this->formulaire($token,$item->id);
                break;
            case 404:
                $content.=$this->listeNoTrouver();
            break;
            case 403:
                $content.=$this->itemNoTrouver();
            break;
            case 402:
                $content.=$this->NoListesCrees();
            break;
            case 401:
                $content.=$this->NoListesParticipee();
            break;
        }
        if ($this->role==0){
            $app = \Slim\Slim::getInstance();
            $url = $app ->urlFor('choix_liste');
            $this->menu="<a href='$url'>Changer de liste</a>";
        }
        $html = <<<FIN
                    $content
                </body>
            </html>
FIN;
        return $html;
    }
}
