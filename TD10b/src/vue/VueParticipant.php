<?php
// vue pour les particiants aux listes
namespace mywishlist\vue;

class VueParticipant{
  const LISTE_ITEM = 1;
  const LISTE_ET_IT= 2;
  const ITEM = 3;
  protected $tabobjet;
  private $select;

  public function __construct($tab, $select){
      $this->tabobjet=$tab;
	    $this->select=$select;
  }

  private function afficheListeListe(): string {
    $div=  '<div class="section"><div id= afficheListeListe>';
  	foreach($this->tabobjet as $var) {
  		$div= $div.'<table border= 1>
          			<tr>
            			<th width=200px>'.$var->no." ".'</th>
            			<th width=200px>'.$var->user_id." ".'</th>
            			<th width=200px>'.$var->titre." ".'</th>
            			<th width=200px>'.$var->description." ".'</th>
            			<th width=200px>'.$var->expiration." ".'</th>
            			<th width=200px>'.$var->token." ".'</th>
          			</tr>
      		 </table>';
	}
	$div= $div.'</div></div>';
	return $div;
}

  private function afficherItemListe():string{
    $res='<div id=afficherItemListe>';
    $l= $this->tabobjet[0];
    $res=$res.'<table border= 1>
                    <tr>
                      <th width=200px>'.$l->no." ".'</th>
                      <th width=200px>'.$l->user_id." ".'</th>
                      <th width=200px>'.$l->titre." ".'</th>
                      <th width=200px>'.$l->description." ".'</th>
                      <th width=200px>'.$l->expiration." ".'</th>
                      <th width=200px>'.$l->token." ".'</th>
                    </tr>
              </table>';
    $item = $l->item()->get();
    foreach ($item as $var) {
      $res=$res.'<table border= 1>
              <tr>
                <th width=200px>'.$var->id." ".'</th>
                <th width=200px>'.$var->liste_id." ".'</th>
                <th width=200px>'.$var->nom." ".'</th>
                <th width=200px>'.$var->descr." ".'</th>
                <th width=200px><img src=img/'.$var->img.' height=200px width= 200px /></th>
                <th width=200px>'.$var->url." ".'</th>
                <th width=200px>'.$var->tarif." ".'</th>
              </tr>
          </table>';
        }
    return $res;
  }

  private function afficherItem() : String{
    $res='<div id=afficherItem>';
    $var= $this->tabobjet[0];
    $res=$res.'<table border= 1>
              <tr>
                <th width=200px>'.$var->id." ".'</th>
                <th width=200px>'.$var->liste_id." ".'</th>
                <th width=200px>'.$var->nom." ".'</th>
                <th width=200px>'.$var->descr." ".'</th>
                <th width=200px><img src=img/'.$var->img.' height=200px width= 200px /></th>
                <th width=200px>'.$var->url." ".'</th>
                <th width=200px>'.$var->tarif." ".'</th>
              </tr>
            </table>';
    return $res;
  }



  public function render($const){
      switch ($const) {
        case LIST_ITEM: {
          $content = $this->afficheListeListe();
          break;
        }
        case LISTE_ET_IT: {
          if (!isnull($this->tabobjet[0])) {
            $content = $this->afficherItemListe();
          }
          break;
        }
        case ITEM: {
          if (!isnull($this->tabobjet[0]) && !isnull($this->tabobjet[1])){
            $content = $this->afficherItem();
            }
          break;
        }
      }
      $html = <<<END
      <!DOCTYPE html> <html>
      <head>  </head> <body>
      <div class="content">
      $content
      </div> </body><html>
  END;
      return $html;
     }
}
