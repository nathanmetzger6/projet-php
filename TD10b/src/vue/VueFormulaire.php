<?php
// la vue pour les formulaires
namespace mywishlist\vue;
use \mywishlist\modele\Item;
use \mywishlist\modele\Compte;
use Slim\Slim;

class VueFormulaire{
    const AFFICHELISTE=0;
    const ADJITEMS=2;
    protected $html;
    protected $role;
    protected $liste;

    public function __construct($r=1, $liste=null){
        $this->role=$r;
        $this->liste=$liste;
    }

    public function afficherFormulaire(){
        $app = \Slim\Slim::getInstance();
        $url = $app ->urlFor('PostListe');
        return <<<FIN
        <div class="section">
            <form class="formL" method="post" action="$url">
            <h2> Créer une liste </h2>
                <p> Nom de la liste : <input type="text" name="titre" required/> </p>
                <p> Description de la liste : <input type="text" name="description" required/></p>
                <p> Date : <input type="date" name="expiration" required/> </p>
                <input type="submit" value="Créer" class="button">
            </form>
            </div>
FIN;
    }

    public function ajouterItem($select=0, $CSSClass="adjItem"){
        $app = \Slim\Slim::getInstance();
        $url = $app ->urlFor('racine');
        $urlP =$url."liste/".$this->liste->token;
        $url = $url."liste/".$this->liste->user_id;
        $lien=<<<FIN
            <h3>
                votre lien de partage est <a href="$urlP">$urlP</a>
            </h3>
FIN;
        $form=
        <<<FIN
            <div class="section">
                <form class="$CSSClass" method="post" >
                    <p> Nom de l'item <input type="text" name="nom" /> </p>
                    <input name="img" type="file" />
                    <input type="submit" value="ajouter" class="button">
                </form>
            </div>
            </form>
FIN;
        return ($select == 0)? $lien.$form: $form;
    }

    public function modifierItem(Item $item,$token){
        $app=Slim::getInstance();
        $routeImg=$app->urlFor('racine')."img/".$item->img;
        $header=header::header();
        $url = $app ->urlFor('racine');
        $url.="modifItem/".$token."/".$item->id;
        return <<<FIN
    $header
    <div class="section">
    <div class="test">
    <form enctype="multipart/form-data" class="modifItem" method="post" action="$url">
        <img src="$routeImg">
        <input name="image" type="file" />
        <p>$item->nom          <input type="text" name="nom" /></p>
        <p>$item->descr        <input type="text" name="description" /></p>
        <p>$item->tarif        <input type="text" name="prix" /></p>
        <input type="submit" value="modifier" class="button">
    </form>
    </div>
</body>
</html>
FIN;
    }

    public function render($select=0){
        $content=header::header();
        switch($select){
            case 0 :
                $content.=$this->afficherFormulaire();
            break;
            case 2 :
                $content.= '<h1> Bravo vous venez de creer votre liste! </h1>';
                $content.= $this->ajouterItem();
            break;
        }
        if ($this->role==0){
            $app = \Slim\Slim::getInstance();
            $url = $app ->urlFor('choix_liste');
            $this->menu="<a href='$url'>Changer de liste</a>";
        }
        $html = <<<FIN
                    $content
                </body>
            </html>
FIN;
        return $html;
    }

    public function inscription(){
        $app = \Slim\Slim::getInstance();
        $url = $app ->urlFor('racine')."Inscription";
        $policy = new \PasswordPolicy\Policy;
        $policy->contains('alnum', $policy->atLeast(8));
        $js = $policy->toJavaScript();
        $content=header::header();
        $content.= <<<FIN
              <div class="section">
                <form class="inscription" method="post" action="$url">
                    <h2>Inscription</h2>
                    <p> Nom : <input type="text" name="nom" /> </p>
                    <p> Prenom : <input type="text" name="prenom" /> </p>
                    <p> Nom de l'utilisateur : <input type="text" name="nomUtil" /> </p>
                    <p> Email : <input type="text" name="Email" /> </p>
                    <p id="mdpVerif"> Mot de passe : <input type="password" name="MDP1" id="mdp" /> </p>
                    <div id="indication"> <h4>le mot de passe doit contenir:</h4>
                    <p id="l">Au minimum 8 charactere</p>
                    <p id="Maj">Une majuscule</p>
                    <p id="low">Une minuscule</p>
                    <p id="chiffre">Un chiffre</p>
                    </div>
                    <p> Retaper votre Mot de passe : <input type="password" name="MDP2" id="mdp2"/> </p>
                    <div id="MDPverifErreur">Les mots de passes ne sont pas identiques</div>
                    <p> Date de naissance : <input type="date" name="DateA" /> </p>
                    <input type="hidden" value="valider" class="button" id="bouton">
                    <script type="text/javascript">

                    var input = document.getElementById("mdp");
                    var input2 = document.getElementById("mdp2")
                    var contrainte = document.getElementById("indication");
                    var policy = $js;
                    contrainte.classList.add("hide");
                    input.onfocus= function(){
                        contrainte.classList.remove("hide");
                        contrainte.classList.add("show");
                    }
                    input.onblur= function(){
                        var password=document.getElementById("mdp").value;
                        var result = policy(password);
                        if (!result.result){
                            contrainte.classList.remove("show");
                            contrainte.classList.add("showErreur");
                        }
                        else{
                            contrainte.classList.remove("show");
                            contrainte.classList.add("hide");
                        }
                    }
                    input2.onblur= function(){
                        if (document.getElementById("mdp").value!=document.getElementById("mdp2").value){
                            document.getElementById("MDPverifErreur").style.display="inline-block";
                        }
                        else{
                            document.getElementById("MDPverifErreur").style.display="none";
                            document.getElementById("bouton").type="submit";
                        }
                    }
                    </script>
                </form>
              </div>
        </body>
        </html>
FIN;
        return $content;
    }

    public function connexion(){
        $app = \Slim\Slim::getInstance();
        $url = $app ->urlFor('racine')."Connexion";
        $content=header::header();
        $content.= <<<FIN
              <div class="section">
                <form class="connexion" method="post" action="$url">
                    <p> Nom de l'utilisateur : <input type="text" name="nomUtil" required/> </p>
                    <p> Mot de passe : <input type="password" name="MDP" id="mdp" required/> </p>
                    <input type="submit" value="valider" class="button" id="bouton">
                </form>
              </div>
        </body>
        </html>
FIN;
        return $content;
    }

    public function deconnexion(){
        $app = \Slim\Slim::getInstance();
        $url = $app ->urlFor('racine')."Deconnexion";
        $content=header::header();
        $content.= <<<FIN
              <div class="section">
                <form class="deconnexion" method="post" action="$url">
                    <p> Vous aller etre deconnecter </p>
                    <input type="submit" value="ok" class="button" >
                </form>
              </div>
        </body>
        </html>
FIN;
        return $content;
    }

    public function monCompte(){
        $app = \Slim\Slim::getInstance();
        $url = $app ->urlFor('racine')."Compte";
        $nomUtil=$_SESSION['compte'];
        $compte= Compte::where('nomUtil','=',$nomUtil)->first();
        $content=header::header();
        $content.= <<<FIN
            <div class="section">
                <form class="compte" method="post" action="$url">
                    <p> Votre compte </p>
                    <p> Nom de l'utilisateur : $nomUtil </p>
                    <p> Nom : $compte->nom  </p>
                    <p> Prenom : $compte->prenom </p>
                    <p> Email : $compte->email </p>
                    <p> Mot de passe : caché </p>
                    <p> Date de naissance : $compte->anniversaire </p>
                    <input type="submit" value="modifier" class="button" >
                </form>
              </div>
        </body>
        </html>
FIN;
    return $content;
    }

    public function modifCompte(){
        $app = \Slim\Slim::getInstance();
        $url = $app ->urlFor('racine')."ModifCompte";
        $nomUtil=$_SESSION['compte'];
        $policy = new \PasswordPolicy\Policy;
        $policy->contains('alnum', $policy->atLeast(8));
        $js = $policy->toJavaScript();
        $compte= Compte::where('nomUtil','=',$nomUtil)->first();
        $content=header::header();
        $content.=<<<FIN
              <div class="section">
                <form class="compte" method="post" action="$url">
                    <p> Nom de l'utilisateur : $nomUtil</p>
                    <p> Nom :  <input type="text" value=$compte->nom name="nom" /></p>
                    <p> Prenom : <input type="text" value=$compte->prenom name="prenom" /> </p>
                    <p> Email : <input type="text" value=$compte->email name="mail" /> </p>
                    <p> Date de naissance : <input type="date" value=$compte->anniversaire name="anniv" /></p>
                    <p id="mdpVerif"> Nouveau mot de passe : <input type="password" name="MDP1" id="mdp" /> </p>
                    <div id="indication"> <h4>le mot de passe doit contenir:</h4>
                    <p id="l">Au minimum 8 charactere</p>
                    <p id="Maj">Une majuscule</p>
                    <p id="low">Une minuscule</p>
                    <p id="chiffre">Un chiffre</p>
                    </div>
                    <p> Retaper votre nouveau mot de passe : <input type="password" name="MDP2" id="mdp2"/> </p>
                    <div id="MDPverifErreur">Les mots de passes ne sont pas identiques</div>
                    <p> Taper votre ancien Mot de passe : <input type="password" name="MDP0" /> </p>
                    <input type="submit" value="valider" class="button" >

                    <script type="text/javascript">

                    var input = document.getElementById("mdp");
                    var input2 = document.getElementById("mdp2")
                    var contrainte = document.getElementById("indication");
                    var policy = $js;
                    contrainte.classList.add("hide");
                    input.onfocus= function(){
                        contrainte.classList.remove("hide");
                        contrainte.classList.add("show");
                    }
                    input.onblur= function(){
                        var password=document.getElementById("mdp").value;
                        var result = policy(password);
                        if (!result.result){
                            contrainte.classList.remove("show");
                            contrainte.classList.add("showErreur");
                        }
                        else{
                            contrainte.classList.remove("show");
                            contrainte.classList.add("hide");
                        }
                    }
                    input2.onblur= function(){
                        if (document.getElementById("mdp").value!=document.getElementById("mdp2").value){
                            document.getElementById("MDPverifErreur").style.display="inline-block";
                        }
                        else{
                            document.getElementById("MDPverifErreur").style.display="none";
                            document.getElementById("bouton").type="submit";
                        }
                    }
                    </script>

                </form>
              </div>
        </body>
        </html>
FIN;
    return $content;
    }

    public function lierListe(){
      $app = \Slim\Slim::getInstance();
      $url = $app ->urlFor('racine')."Compte";
      $nomUtil=$_SESSION['compte'];
      $compte= Compte::where('nomUtil','=',$nomUtil)->first();
      $content=header::header();
      $content.= <<<FIN
          <div class="section">
              <form class="lierliste" method="post" action="$url">
                  <p> Url de la liste  : <input type="text" name="urlliste" required/> </p>
                  <input type="submit" value="Lier au compte" class="button" >
              </form>
            </div>
      </body>
      </html>
FIN;
    return $content;
  }
}
