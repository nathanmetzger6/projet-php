<?php
// le header du site qui est present sur toutes les pages
namespace mywishlist\vue;
class Header{
    public static function header($select=1){
      $app = \Slim\Slim::getInstance();
      $route=$app->urlFor("racine");
      $routeCreer=$route."creerListe";
      $routeConnexion=$route.'Connexion';
      $routeInscription=$route.'Inscription';
      $routeListePub=$route.'ListePublique';
      $routeDeco=$route.'Deconnexion';
      $routeCompte=$route.'Compte';
      $routeListesCree=$route.'ListeCree';
      $routeListespart=$route.'ListeParticip';
      if(isset($_SESSION[ 'compte' ])){
        $content= <<<FIN
        <!DOCTYPE html>
        <html>
        <head>

        <meta charset="utf-8"/>
            <title>My WishList</title>
            <link rel="stylesheet" href="$route/app.css">
        </head>
FIN;
        if ($select==1){
          $content.='<body><header class="header">';
        }
        else {
          $content.='<body class="body"><header class="headerIndex">';
        }
        return $content . <<<FIN
                        <a href="$route"<h1 class="titreH"> MyWishList</h1></a>
                            <ul class="menu">
                                <li><a href="$routeCreer"> Créer une liste</a></li>
                                <li><a href="$routeListePub"> Listes publiques</a></li>
                                <li><a href="$routeListesCree"> Mes listes</a></li>
                                <li><a href="$routeListespart"> Mes participations</a></li>
                                <li><a href="$routeCompte"> Mon compte</a></li>
                                <li><a href="$routeDeco"> Deconnexion</a></li>
                            </ul>
                            <script type="text/javascript">

</script>
                    </header>


FIN;
      }else{
        $content= <<<FIN
        <!DOCTYPE html>
        <html>
        <head>

        <meta charset="utf-8"/>
            <title>My WishList</title>
            <link rel="stylesheet" href="$route/app.css">
        </head>
FIN;
        if ($select==1){
          $content.='<body><header class="header">';
        }
        else {
          $content.='<body class="body"><header class="headerIndex">';
        }
        return $content . <<<FIN
                        <a href="$route"<h1 class="titreH"> MyWishList</h1></a>
                            <ul class="menu">
                                <li><a href="$routeCreer"> Créer une liste</a></li>
                                <li><a href="$routeListePub"> Listes publiques</a></li>
                                <li><a href="$routeInscription"> Inscription</a></li>
                                <li><a href="$routeConnexion"> Connexion</a></li>
                            </ul>
                            <script type="text/javascript">

</script>
                    </header>


FIN;
    }
  }
}
